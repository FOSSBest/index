<?php

// Include composer dependencies
include_once __DIR__ . '/../vendor/autoload.php';
use Symfony\Component\Yaml\Yaml;

// Initialize variables
$dirIndex = "index/";
$index = array();

// Scan all files inside the index folder
$files = scandir($dirIndex);

// For each YAML file
foreach($files as $file) {
    // Skip . and ..
    if (($file == '.') || ($file == '..')) {
        continue;
    }

    // Read YAML file
    $item = Yaml::parseFile($dirIndex.$file);

    // Append to index array
    $index[$item["id"]] = $item;

}
// Inspect data in console
// print_r($index);

// Save to JSON file
$fp = fopen('dist/index.json', 'w');
fwrite($fp, json_encode($index));
fclose($fp);

?>
